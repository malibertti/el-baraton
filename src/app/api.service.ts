import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Category } from './model/category';
import { Product, JSONProduct } from './model/product';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }

  // Returns a categories collection.
  public getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>('/api/v1/categories');
  }

  // Returns a products collection.
  public getProducts(categoryId?: number): Observable<Product[]> {
    const url = categoryId ? `/api/v1/categories/${categoryId}/products` : `/api/v1/products`;

    return this.http.get<JSONProduct[]>(url).pipe(
      map(data => data.map(i => this.normalizeProduct(i))),
    );
  }

  // Returns a single product.
  public getProduct(id: string): Observable<Product> {
    return this.http.get<JSONProduct>(`/api/v1/products/${id}`).pipe(
      map(data => this.normalizeProduct(data)),
    );
  }

  // Converts price to number and ads a dummy image.
  private normalizeProduct(product: JSONProduct): Product {
    const price = this.stringPriceToNumber(product.price);
    const image = '/assets/food-2.jpg';
    return { ...product, price, image };
  }

  private stringPriceToNumber(price: string): number {
    const num = price.replace('$', '').replace(',', '.');
    return parseFloat(num);
  }
}
