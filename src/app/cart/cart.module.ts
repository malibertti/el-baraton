import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { AddToCartComponent } from './add-to-cart/add-to-cart.component';
import { ProductResolver } from '../products/product.resolver';
import { CartComponent } from './cart/cart.component';
import { QuantityControlComponent } from './quantity-control/quantity-control.component';
import { CartItemComponent } from './cart-item/cart-item.component';
import { OrderSuccessComponent } from './order-success/order-success.component';

const routes: Routes = [
  {
    path: 'add/:id',
    outlet: 'modal',
    component: AddToCartComponent,
    resolve: { product: ProductResolver },
  },
  {
    path: 'cart',
    outlet: 'aside',
    component: CartComponent,
  },
  {
    path: 'order-success',
    outlet: 'modal',
    component: OrderSuccessComponent,
  },
];

@NgModule({
  declarations: [
    AddToCartComponent,
    CartComponent,
    QuantityControlComponent,
    CartItemComponent,
    OrderSuccessComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    SharedModule,
  ],
  exports: [
    AddToCartComponent,
  ],
})
export class CartModule { }
