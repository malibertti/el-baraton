import { Component, ChangeDetectionStrategy, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'eb-quantity-control',
  templateUrl: './quantity-control.component.html',
  styleUrls: ['./quantity-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: forwardRef(() => QuantityControlComponent) },
  ],
})
export class QuantityControlComponent implements ControlValueAccessor {

  @Input() max: number;
  public quantity: number;
  public onChange: (val: any) => void;

  /**
   * Updates quantity and fires and notify the form.
   */
  public update(num: number): void {
    this.quantity += num;
    this.onChange(this.quantity);
  }

  /**
   * Writes a new value to the element.
   */
  public writeValue(num: number): void {
    if (num) {
      this.quantity = num;
    }
  }

  /**
   * Registers a callback function that is called when the control's value changes in the UI.
   */
  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * Registers a callback function is called by the forms API on initialization to update the form model on blur.
   */
  public registerOnTouched(_fn: any): void {
    // Not required.
  }

  /**
   * Function that is called by the forms API when the control status changes to or from 'DISABLED'.
   * Depending on the status, it enables or disables the appropriate DOM element.
   */
  public setDisabledState?(_isDisabled: boolean): void {
    // Not required.
  }
}
