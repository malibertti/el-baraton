import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'eb-order-success',
  templateUrl: './order-success.component.html',
  styleUrls: ['./order-success.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderSuccessComponent { }
