import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { Product } from 'src/app/model/product';
import { map, filter, tap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CartService } from '../cart.service';

@Component({
  selector: 'eb-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddToCartComponent implements OnInit {

  public product$: Observable<Product>;
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private cartService: CartService,
  ) { }

  /**
   * Binds product from route resolver and init the form.
   */
  ngOnInit() {
    this.product$ = this.activatedRoute.data.pipe(
      map(data => data['product']),
      filter(item => !!item),
      tap((item) => this.initForm(item)),
    );
  }

  /**
   * Handles form submit adding an OrderItem to the Cart.
   */
  public onSubmit(product: Product, form: FormGroup): void {
    const quantity = form.get('quantity').value;
    this.cartService.add(product.id, quantity);
  }

  private initForm(product: Product): void {
    this.form = this.formBuilder.group(
      {
        quantity: [1, [
          Validators.required,
          Validators.min(1),
          Validators.max(product.quantity),
        ]],
      },
    );
  }

}
