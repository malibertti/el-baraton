import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { OrderProduct, OrderItem } from 'src/app/model/order';
import { CartService } from '../cart.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'eb-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartItemComponent implements OnInit, OnDestroy {

  @Input() item: OrderProduct;
  public form: FormGroup;
  private formSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private cartService: CartService,
  ) { }

  /**
   * Init form and subscribe.
   */
  public ngOnInit(): void {
    this.formSubscription = this.initForm(this.item);
  }

  /**
   * Unsubscribe on destroy.
   */
  public ngOnDestroy(): void {
    this.formSubscription.unsubscribe();
  }

  private initForm(item: OrderProduct): Subscription {

    this.form = this.formBuilder.group(
      {
        quantity: [item.quantity],
      },
    );

    return this.form.get('quantity').valueChanges
      .subscribe(val => {
        const orderItem: OrderItem = {
          id: item.id,
          productId: item.product.id,
          quantity: val,
        };

        this.cartService.update(orderItem);
      });
  }

}
