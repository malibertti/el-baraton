import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { Guid } from 'guid-typescript';

import { Order, OrderItem, OrderProduct } from '../model/order';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

/**
 * The Cart Store:
 * Implements a private Subject to handle data internally and exposes an Observable
 * to be subscribed by any other part of the application.
 * The data is stored on localStorage.
 */
@Injectable({
  providedIn: 'root',
})
export class CartService {

  private _store: BehaviorSubject<OrderItem[]> = new BehaviorSubject(this.storage);
  public store$: Observable<Order>;

  constructor(
    private router: Router,
    private apiService: ApiService,
  ) {
    this.store$ = this.buildStore();
  }

  /**
   * Creates an OrderItem and adds it to the collection.
   */
  public add(productId: string, quantity: number) {
    const currentItems = this._store.getValue();
    const id = Guid.create().toString();
    const newItem: OrderItem = { id, productId, quantity };

    this.storage = [...currentItems, newItem];
    this.closeOutlet('modal');
  }

  /**
   * Removes an OrderItem from the collection.
   */
  public remove(id: string): void {
    const currentItems = this._store.getValue();
    this.storage = currentItems.filter(i => i.id !== id);
  }

  /**
   * Updates an OrderItem from the collection.
   */
  public update(orderItem: OrderItem): void {
    const currentItems = this._store.getValue();
    this.storage = currentItems.map(item => (item.id === orderItem.id) ? orderItem : item);
  }

  /**
   * Confirms a purchase, cleaning the storage and displaying a success modal.
   */
  public async complete(order: Order) {
    // #todo: Display order data on the success modal.
    console.log('ORDER SUCCESS', order);

    this.storage = [];
    await this.closeOutlet('aside');
    this.router.navigate([{ outlets: { modal: ['order-success'] } }]);
  }

  /**
   * Closes a router outlet by navigation.
   */
  private closeOutlet(outlet: 'aside' | 'modal'): Promise<Boolean> {
    return this.router.navigate([{ outlets: { [outlet]: null } }]);
  }

  /**
   * A setter to save to localStorage and emit a new value to the stream.
   */
  private set storage(items: OrderItem[]) {
    window.localStorage.setItem('cart', JSON.stringify(items));
    this._store.next(items);
  }

  /**
   * A getter to safe-get the value of localStorage.
   */
  private get storage() {
    const data = window.localStorage.getItem('cart');
    return JSON.parse(data) || [];
  }

  /**
   * Builds the public store combining the list of products from the server and the OrderItem collection.
   * This is read-only and any component or service in the app can subscribe to get the hot stream of data.
   */
  private buildStore(): Observable<Order> {
    const products$ = this.apiService.getProducts();
    const store$ = this._store.asObservable();

    return combineLatest(products$, store$).pipe(
      map(([products, orders]): OrderProduct[] => {
        return orders.map(orderItem => {
          const product = products.find(p => p.id === orderItem.productId);
          const quantity = orderItem.quantity;
          const id = orderItem.id;

          return { id, product, quantity };
        });
      }),
      map(items => {
        const total = items.reduce((acc, item) => {
          acc += (item.quantity * item.product.price);
          return acc;
        }, 0);

        return { items, total };
      }),
    );
  }

}
