import { Component, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

import { CartService } from '../cart.service';
import { OrderProduct } from 'src/app/model/order';

@Component({
  selector: 'eb-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('row', [
      transition(':leave', [
        style({ opacity: 1 }),
        animate('250ms', style({ opacity: 0 })),
      ]),
    ]),
  ],
})
export class CartComponent {

  constructor(
    public cartService: CartService,
  ) { }

  /**
   * Track ngFor items by order ID.
   */
  public trackByFn(_index: number, item: OrderProduct) {
    return item.id;
  }

}
