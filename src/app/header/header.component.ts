import { Component, ChangeDetectionStrategy, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CartService } from '../cart/cart.service';

@Component({
  selector: 'eb-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {

  @Output() public toggleAside: EventEmitter<boolean> = new EventEmitter();
  public orderItems$: Observable<number>;

  constructor(
    private cartService: CartService,
  ) { }

  /**
   * Subscribe to the Cart store to bind the orders count.
   */
  public ngOnInit(): void {
    this.orderItems$ = this.cartService.store$.pipe(
      map(order => order.items.length),
    );
  }

  /**
   * Emits an event on sidebar icon clicked.
   */
  public onToggle(): void {
    this.toggleAside.emit();
  }

}
