import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Category } from '../model/category';
import { ApiService } from '../api.service';

@Component({
  selector: 'eb-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent implements OnInit {

  public categories$: Observable<Category[]>;

  constructor(
    private apiService: ApiService,
  ) { }

  /**
   * Fetch a collection of categories from the server and binds it to the template.
   */
  public ngOnInit(): void {
    this.categories$ = this.apiService.getCategories();
  }

}
