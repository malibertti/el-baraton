import { Product } from './product';

/**
 * Describes the base of an Order.
 */
interface OrderBase {
  id: string;
  quantity: number;
}

/**
 * Describes an order with a product id.
 */
export interface OrderItem extends OrderBase {
  productId: string;
}

/**
 * Describes an order with a product object.
 */
export interface OrderProduct extends OrderBase {
  product: Product;
}

/**
 * Describes an Order with a collection of OrderProduct and the total amount.
 */
export interface Order {
  items: OrderProduct[];
  total: number;
}
