/**
 * Describes the base or a Product.
 */
interface ProductBase {
  id: string;
  available: boolean;
  name: string;
  quantity: number;
  sublevel_id: number;
}

/**
 * Describes the Product from the server.
 */
export interface JSONProduct extends ProductBase {
  price: string;
}

/**
 * Describes the complete Product.
 */
export interface Product extends ProductBase {
  price: number;
  image: string;
}

/**
 * Describes the available filters for a Product collection.
 */
export interface ProductFilters {
  available: boolean;
  quantity: number;
  price: { min: number, max: number };
}

/**
 * Describes the available sirting for a Product collection.
 */
export interface ProductSorting {
  key: string;
  order: string;
  label: string;
}
