import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Router, RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'eb-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingComponent implements OnInit {

  public loading$: Observable<boolean>;

  constructor(
    private router: Router,
  ) { }

  /**
   * Initializes the svg dimensions and subscribe to router events.
   */
  public ngOnInit(): void {
    this.loading$ = this.router.events.pipe(
      map((ev: RouterEvent) => this.handleRouterEvent(ev)),
      filter(val => typeof val === 'boolean'),
    );
  }

  /**
   * Returns true on navigation start and false on (end, cancel, error).
   */
  private handleRouterEvent(ev: RouterEvent): boolean {
    if (ev instanceof NavigationStart) {
      return true;
    }

    if (ev instanceof NavigationEnd ||
      ev instanceof NavigationCancel ||
      ev instanceof NavigationError) {
      return false;
    }
  }
}
