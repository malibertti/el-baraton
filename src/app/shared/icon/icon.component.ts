import { Component, ChangeDetectionStrategy, Input, OnInit } from '@angular/core';

@Component({
  selector: 'eb-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent implements OnInit {

  @Input() name: string;
  @Input() size = '24';

  public width: string;
  public height: string;

  get absUrl() {
    return window.location.href;
  }

  ngOnInit() {
    this.width = this.size;
    this.height = this.size;
  }

}
