import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconDefsComponent } from './icon-defs.component';

describe('IconDefsComponent', () => {
  let component: IconDefsComponent;
  let fixture: ComponentFixture<IconDefsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconDefsComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconDefsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
