import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'eb-icon-defs',
  templateUrl: './icon-defs.component.html',
  styleUrls: ['./icon-defs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconDefsComponent { }
