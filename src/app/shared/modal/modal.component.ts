import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'eb-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent {

  @Output() closed: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router,
  ) { }

  public close(): void {
    this.router.navigate([{ outlets: { modal: null } }]);
    this.closed.next();
  }

}
