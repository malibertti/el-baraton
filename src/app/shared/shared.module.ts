import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalComponent } from './modal/modal.component';
import { AsideComponent } from './aside/aside.component';
import { LoadingComponent } from './loading/loading.component';
import { IconComponent } from './icon/icon.component';
import { IconDefsComponent } from './icon-defs/icon-defs.component';

@NgModule({
  declarations: [
    LoadingComponent,
    ModalComponent,
    AsideComponent,
    IconComponent,
    IconDefsComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    LoadingComponent,
    ModalComponent,
    AsideComponent,
    IconComponent,
    IconDefsComponent,
  ],
})
export class SharedModule { }
