import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'eb-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AsideComponent {

  @Output() closed: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router,
  ) { }

  public close(): void {
    this.router.navigate([{ outlets: { aside: null } }]);
    this.closed.next();
  }

}
