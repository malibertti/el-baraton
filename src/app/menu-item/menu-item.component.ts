import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Category } from '../model/category';

@Component({
  selector: 'eb-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuItemComponent {

  @Input() public category: Category;

}
