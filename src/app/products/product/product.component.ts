import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Product } from 'src/app/model/product';

@Component({
  selector: 'eb-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductComponent {

  @Input() public product: Product;

}
