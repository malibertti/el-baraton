import { Pipe, PipeTransform } from '@angular/core';

import { Product, ProductFilters } from 'src/app/model/product';

@Pipe({
  name: 'filterBy',
})
export class FilterByPipe implements PipeTransform {

  // TODO: Make this filter generic, to extend gracefully.
  transform(products: Product[], args: ProductFilters): any {
    const filters = Object.keys(args);

    return products.filter(product => {
      return filters.every(key => {

        if (key === 'available' && args[key] !== null) {
          return product[key] === args[key];
        }

        if (key === 'quantity' && args[key] !== null) {
          return product[key] >= args[key];
        }

        if (key === 'price') {
          const min = args[key].min || 0;
          const max = args[key].max || Infinity;
          const price = product[key];

          return price >= min && price <= max;
        }

        return true;
      });
    });
  }

}
