import { Pipe, PipeTransform } from '@angular/core';

import { Product, ProductSorting } from 'src/app/model/product';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {

  transform(products: Product[], args: ProductSorting): any {

    if (args === null) {
      return products;
    }

    const { order, key } = args;

    return products.sort((a: Product, b: Product) => {

      if (typeof a[key] === 'number') {
        if (order === 'ASC') {
          return a[key] - b[key];
        } else {
          return b[key] - a[key];
        }
      }

      if (typeof a[key] === 'boolean') {
        if (order === 'ASC') {
          return a[key] - b[key];
        } else {
          return b[key] - a[key];
        }
      }

    });
  }
}
