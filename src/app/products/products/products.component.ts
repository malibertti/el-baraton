import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, tap, filter } from 'rxjs/operators';

import { Product, ProductFilters, ProductSorting } from 'src/app/model/product';

@Component({
  selector: 'eb-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductsComponent implements OnInit {

  public isFiltersFormOpen = false;
  public products$: Observable<Product[]>;
  public filterOptions$: Observable<ProductFilters>;
  public sortingOptions$: Observable<ProductSorting>;
  public formFilters: FormGroup;
  public formSorting: FormGroup;
  public sortingOptions: ProductSorting[] = [
    { key: 'price', order: 'ASC', label: 'Más baratos' },
    { key: 'price', order: 'DESC', label: 'Más costosos' },
    { key: 'available', order: 'ASC', label: 'Menor disponibilidad' },
    { key: 'available', order: 'DESC', label: 'Mayor disponibilidad' },
    { key: 'quantity', order: 'ASC', label: 'Menor Stock' },
    { key: 'quantity', order: 'DESC', label: 'Mayor Stock' },
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
  ) { }

  /**
   * Binds products collection from route resolver and init the forms.
   */
  public ngOnInit(): void {
    this.products$ = this.activatedRoute.data.pipe(
      map(data => data['products']),
      filter(items => items.length),
      tap(() => this.initForms()),
    );
  }

  /**
   * Calculates the max quantity from the reduced collection.
   */
  public getMaxQuantity(products: Product[]): number {
    return products.reduce((acc, val) => {
      const quantity = val.quantity;
      if (quantity > acc) {
        acc = quantity;
      }
      return acc;
    }, 0);
  }

  private initForms(): void {
    this.formFilters = this.formBuilder.group({
      available: [],
      quantity: [0],
      price: this.formBuilder.group({
        min: [],
        max: [],
      }),
    });

    this.formSorting = this.formBuilder.group({
      orderBy: [],
    });

    this.filterOptions$ = this.formFilters.valueChanges.pipe(
      startWith(this.formFilters.value),
    );

    this.sortingOptions$ = this.formSorting.valueChanges.pipe(
      startWith(this.formSorting.value),
      map(value => value.orderBy),
    );
  }

}
