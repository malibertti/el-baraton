import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Product } from 'src/app/model/product';

@Component({
  selector: 'eb-products-grid',
  templateUrl: './products-grid.component.html',
  styleUrls: ['./products-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductsGridComponent {

  @Input() products: Product[];

}
