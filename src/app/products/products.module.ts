import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { OrderByPipe } from './pipes/order-by.pipe';
import { FilterByPipe } from './pipes/filter-by.pipe';
import { SharedModule } from '../shared/shared.module';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { ProductsResolver } from './products.resolver';
import { ProductsGridComponent } from './products-grid/products-grid.component';

const routes: Routes = [
  {
    path: ':id',
    component: ProductsComponent,
    resolve: { products: ProductsResolver },
  },
  {
    path: '',
    component: ProductsComponent,
    resolve: { products: ProductsResolver },
  },
];

@NgModule({
  declarations: [
    OrderByPipe,
    FilterByPipe,
    ProductsComponent,
    ProductComponent,
    ProductsGridComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
})
export class ProductsModule { }
