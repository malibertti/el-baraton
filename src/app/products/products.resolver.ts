import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';

import { Product } from '../model/product';

@Injectable({
  providedIn: 'root',
})
export class ProductsResolver implements Resolve<Product[]> {

  constructor(
    private apiService: ApiService,
  ) { }

  /**
   * Fetch a collection of products from the server.
   */
  resolve(route: ActivatedRouteSnapshot): Observable<Product[]> {
    const id = route.paramMap.get('id');
    return this.apiService.getProducts(+id);
  }
}
