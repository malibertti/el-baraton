const Drydock = require('drydock');
const categories = require('./categories').categories;
const products = require('./products').products;

// Drydock mock server instance.
const drydock = new Drydock({
  initialState: { categories, products },
  cors: true,
  verbose: true,
});

// Routing
const basePath = '/api/v1';
const HttpError = Drydock.Errors.HttpError;

drydock.jsonRoute({
  name: 'get-categories',
  method: 'GET',
  path: `${basePath}/categories`,
  handlers: {
    'get-categories-success': {
      description: 'Returns a list of categories.',
      handler() {
        return this.state.categories;
      },
    },
    'get-categories-error': {
      description: 'Fails to return a list of categories.',
      handler() {
        throw new HttpError(500, {
          message: 'Get categories failed by testing reasons',
        });
      },
    },
  },
});

drydock.jsonRoute({
  name: 'get-products-by-category',
  method: 'GET',
  path: `${basePath}/categories/{id}/products`,
  handlers: {
    'get-products-success': {
      description: 'Returns a list of products.',
      handler(req) {
        const id = parseInt(req.params.id, 10);
        return this.state.products.filter(product => product.sublevel_id === id);
      },
    },
    'get-products-error': {
      description: 'Fails to return a list of products.',
      handler() {
        throw new HttpError(500, {
          message: 'Get products failed by testing reasons',
        });
      },
    },
  },
});

drydock.jsonRoute({
  name: 'get-products',
  method: 'GET',
  path: `${basePath}/products`,
  handlers: {
    'get-products-success': {
      description: 'Returns a list of products.',
      handler() {
        return this.state.products;
      },
    },
    'get-products-error': {
      description: 'Fails to return a list of products.',
      handler() {
        throw new HttpError(500, {
          message: 'Get product failed by testing reasons',
        });
      },
    },
  },
});

drydock.jsonRoute({
  name: 'get-product',
  method: 'GET',
  path: `${basePath}/products/{id}`,
  handlers: {
    'get-product-success': {
      description: 'Returns a single product.',
      handler(req) {
        const id = req.params.id;
        const product = this.state.products.filter(product => product.id === id);

        if (product.length) {
          return product[0];
        }

        throw new HttpError(404, {
          message: 'Product not found',
        });
      },
    },
    'get-product-error': {
      description: 'Fails to return a single product.',
      handler() {
        throw new HttpError(500, {
          message: 'Get product failed by testing reasons',
        });
      },
    },
  },
});

// Start the server.
drydock.start();
