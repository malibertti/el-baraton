# El Baratón

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Develop](#develop)
- [Mock server](#mock-server)
- [Dev Notes](#dev-notes)
- [Pending Tasks](#pending-tasks)

## Prerequisites

- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [NodeJS with npm](https://nodejs.org/en/)

## Installation

```bash
git clone git@bitbucket.org:malibertti/el-baraton.git
cd el-baraton
npm install
```

## Develop

1. Run `npm run mock` to start the mock server. An API instance will now be running at [http://127.0.0.1:1337/]().
2. Run `npm start` for a dev server.
3. Navigate to `http://127.0.0.1:4200/`. The app will automatically reload if you change any of the source files.

## Mock Server

To check-out which APIs have been mocked and control the behavior of those APIs, visit
the server UI at [http://127.0.0.1:1337/drydock/](). The implementation is based on [drydock](https://github.com/divmain/drydock)

## Dev Notes
- All code, comments and instructions are in English.
- App was developed and tested in macOS Chrome.
- A separate process runs a mock server that mimics a REST API using the provided JSON files.
- A response delay can be set in the mock UI to check the loading indicator.
- The `Cart` contains a collection of `OrderItem` saved on `localStorage`.
`OrderItem` only contains a generated uuid, the product id and quantity.
The `Cart` transforms a collection of `OrderItem` to a collection of `OrderProduct` to keep product items always updated (price, name, image, etc). I can explain this decision in detail verbally.
All price calculations are calculated reactively on data change. 
- The styles are dimmed, only black and white with a brand color. 
- RWD is implemented with the Mobile First approach.
- CSS BEM methodology is used on global styles and some scoped styles too.
- App Layout is built with Flexbox and CSS Grid for products list.

## Pending Tasks

- Testing (unit, integration, e2e).
- Proper error handling.
- Translation & Localization.
- Display purchase in the success modal.
- Implement subtle animations on (modal, sidebar, product filters) open / close.
- Close (modal, sidebar) on outer click.
- Fix small visual bugs in RWD.
