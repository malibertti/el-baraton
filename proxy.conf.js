const PROXY_CONFIG = {
  '/api/v1': {
    target: 'http://127.0.0.1:1337',
    secure: true,
    changeOrigin: true
  },
};

module.exports = PROXY_CONFIG;
